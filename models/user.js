const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const UserSchema = new Schema(
    {
      username: {
        type: String,
        required: [true, 'Username is missing'],
        unique: [true, 'Username already in use'],
      },
      password: {
        type: String,
        required: [true, 'Password is missing'],
      },
      email: {
        type: String,
      },
      firstName: {
        type: String,
      },
      lastName: {
        type: String,
      },
    },
    {timestamps: true},
);

UserSchema.pre('save', async function(next) {
  if (!this.isModified('password')) {
    return next();
  }
  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash(this.password, salt);
  this.password = hashedPassword;
  next();
});

UserSchema.pre('findOneAndUpdate', async function(next) {
  if (this._update.password) {
    const salt = await bcrypt.genSalt();
    const hashed = await bcrypt.hash(this._update.password, salt);
    this._update.password = hashed;
  }
  next();
});

const UserModel = mongoose.model('User', UserSchema);

module.exports.UserModel = UserModel;

