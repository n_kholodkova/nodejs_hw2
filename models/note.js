const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema(
    {
      userId: {
        type: String,
        required: true,
      },
      text: {
        type: String,
        required: [true, 'No note text'],
      },
      completed: {
        type: Boolean,
      },
    },
    {timestamps: true},
);

const NoteModel = mongoose.model('Note', NoteSchema);

module.exports.NoteModel = NoteModel;

