# Very simple server with Node.js and Express.js. Used JWT Authentication.

### Routes:
- #### GET

  - /api/users/me

    return user's data. Requires JWT token in headers.

  - /api/notes

    return list of user's notes. Requires JWT token in headers.

    - /api/notes/:noteid

    return note with given id. If user has not such note, returns status 400 and error message. Requires JWT token in headers.

- #### POST

  - /api/auth/register

    in body of request gets parameters as json in next format {username: "USERNAME", password: "PASSWORD"}. Return error and status code 400 if user with given username already exists, otherwise success message and code 200.

    - /api/auth/login

    in body of request gets parameters as json in next format {username: "USERNAME", password: "PASSWORD"}. Return error and status code 400 if user with given username already exists, otherwise success with JWT token message and code 200.

    - /api/notes

    in body of request gets parameters as json in next format {text: "NOTE TEXT"}. Return error status code 400 if note does not exist. Otherwise success message and code 200 and create new note. Requires JWT token in headers.

- #### PUT

  - /api/notes/:noteid

    in body of request gets parameters as json in next format {text: "NOTE TEXT"}. Return error status code 400 if note does not exist. Otherwise success message and code 200 and update note. Requires JWT token in headers.

- #### PATCH

  - /api/users/me

    in body of request gets parameters as json in next format {oldPassword: "OLDPASSWORD", newPassword: "NEWPASSWORD"}. Return error and status code 400 if user with given username does not exist or oldPassword is invalid, otherwise success message and code 200. Requires JWT token in headers.

  - /api/notes/:noteid

    Return error status code 400 if note does not exist. Otherwise success message and code 200 and update note completed status vise versa. Requires JWT token in headers.

- #### DELETE

    - /api/users/me

    Return success message and code 200 and deletes user. Requires JWT token in headers.

  - /api/notes/:noteid

    return error status code 400 if note does not exist, otherwise success message and code 200. Requires JWT token in headers.

To run use `npm install` and `npm start`
