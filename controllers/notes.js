const {NoteModel} = require('../models/note');

const getNotes = async (request, response) => {
  let {limit, offset} = request.query;
  try {
    limit = parseInt(limit) || 0;
    offset = parseInt(offset) || 0;
  } catch (error) {
    response.status(400).json({message: error.message}).end();
    return;
  }
  try {
    const notesList = await NoteModel.find({userId: request.userId})
        .skip(parseInt(offset))
        .limit(parseInt(limit)) ?? [];
    const notes = Array.from(notesList)
        .map(({_id, userId, createdAt, text, completed}) => (
          {_id, userId, completed, text, createdDate: new Date(createdAt)}
        ));
    response.status(200).json({offset, limit, count: notes.length, notes});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const createNote = async (request, response) => {
  const {text} = request.body;
  try {
    await NoteModel.create({text, userId: request.userId, completed: false});
    response.status(200).json({message: 'Success'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const getNote = async (request, response) => {
  const {id} = request.params;
  try {
    const note = await NoteModel.findOne({_id: id});
    response.status(200).json({
      note: {
        _id: note._id,
        userId: request.userId,
        completed: note.completed,
        text: note.text,
        createdDate: new Date(note.createdAt),
      },
    });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const updateNote = async (request, response) => {
  const {text} = request.body;
  const {id} = request.params;
  if (!text) {
    response.status(400).json({message: 'No text provided'}).end();
    return;
  }
  if (!id) {
    response.status(400).json({message: 'No note id provided'}).end();
    return;
  }
  try {
    await NoteModel.findOneAndUpdate(
        {_id: id}, {text: text},
        {
          runValidators: true,
          context: 'query',
        });
    response.status(200).json({message: 'Success'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  }
};

const updateNoteComplete = async (request, response) => {
  const {id} = request.params;
  if (!id) {
    response.status(400).json({message: 'No note id provided'}).end();
    return;
  }
  try {
    await NoteModel.findOneAndUpdate(
        {_id: id}, {completed: !this.completed},
        {
          runValidators: true,
          context: 'query',
        });
    response.status(200).json({message: 'Success'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  }
};

const deleteNote = async (request, response) => {
  const {id} = request.params;
  if (!id) {
    response.status(400).json({message: 'No note id provided'}).end();
    return;
  }
  try {
    await NoteModel.remove({_id: id});
    response.status(200).json({message: 'Success'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

module.exports.getNotes = getNotes;
module.exports.createNote = createNote;
module.exports.getNote = getNote;
module.exports.updateNote = updateNote;
module.exports.updateNoteComplete = updateNoteComplete;
module.exports.deleteNote = deleteNote;
