const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();
const {UserModel} = require('../models/user');

const JWT_AGE = 24 * 60 * 60;
const createJWTToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: JWT_AGE,
  });
};

const signUp = async (request, response) => {
  const {username, password} = request.body;
  try {
    await UserModel.create({username, password});
    response.status(200).json({message: 'Success'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const signIn = async (request, response) => {
  try {
    const {username, password} = request.body;
    if (!username) {
      response.status(400).json({message: 'No username'}).end();
      return;
    }
    if (!password) {
      response.status(400).json({message: 'No password'}).end();
      return;
    }
    const currentUser = await UserModel.findOne({username});
    if (!currentUser) {
      response.status(400).json({message: 'Invalid username'}).end();
      return;
    }
    const isValidPassword = await bcrypt
        .compare(password, currentUser.password);
    if (!isValidPassword) {
      response.status(400).json({message: 'Invalid password'}).end();
      return;
    }
    const JWTtoken = createJWTToken(currentUser._id);
    response.status(200).json({message: 'Success', jwt_token: JWTtoken});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
    return;
  }
};

module.exports.signUp = signUp;
module.exports.signIn = signIn;
