const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const fs = require('fs');
const cors = require('cors');
require('dotenv').config();
const {dataBase} = require('./config');

const PORT = process.env.PORT || 8080;

const expressApp = express();
expressApp.use(express.json());
expressApp.use(morgan('common', {
  stream: fs.createWriteStream('./logs.log', {flags: 'a'}),
}));
expressApp.use(morgan('dev'));
expressApp.use(cors());

try {
  mongoose.connect(dataBase.uri).then(() => expressApp.listen(PORT, () => {
    console.log(`App is running on port ${PORT}...`);
  }));
} catch (error) {
  console.log('Cannot connect to DB');
}

require('./routes/authRoutes')(expressApp);
require('./routes/meRoutes')(expressApp);
require('./routes/notesRoutes')(expressApp);
