const {getMe, updateMe, deleteMe} = require('./../controllers/me');
const {authMiddleware} = require('../middleware/authMiddleware');

module.exports = (expressApp) => {
  expressApp.get('/api/users/me', authMiddleware, (request, response) => {
    try {
      getMe(request, response);
    } catch (error) {
      response.status(500).json({error: 'Internal server error'});
    }
  });

  expressApp.patch('/api/users/me', authMiddleware, (request, response) => {
    try {
      updateMe(request, response);
    } catch (error) {
      response.status(500).json({error: 'Internal server error'});
    }
  });

  expressApp.delete('/api/users/me', authMiddleware, (request, response) => {
    try {
      deleteMe(request, response);
    } catch (error) {
      response.status(500).json({error: 'Internal server error'});
    }
  });
};
