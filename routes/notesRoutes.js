const {
  getNotes,
  createNote,
  getNote,
  updateNote,
  updateNoteComplete,
  deleteNote} = require('./../controllers/notes');
const {authMiddleware} = require('../middleware/authMiddleware');
const {isUserExists,
  isNoteExists,
  isValidUser} = require('../middleware/notesMiddleware');

module.exports = (expressApp) => {
  expressApp.get('/api/notes',
      authMiddleware,
      isUserExists,
      (request, response) => {
        try {
          getNotes(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });

  expressApp.post('/api/notes',
      authMiddleware,
      isUserExists,
      (request, response) => {
        try {
          createNote(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });

  expressApp.get('/api/notes/:id',
      authMiddleware,
      isUserExists,
      isNoteExists,
      isValidUser,
      (request, response) => {
        try {
          getNote(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });

  expressApp.put('/api/notes/:id',
      authMiddleware,
      isUserExists,
      isNoteExists,
      isValidUser,
      (request, response) => {
        try {
          updateNote(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });

  expressApp.patch('/api/notes/:id',
      authMiddleware,
      isUserExists,
      isNoteExists,
      isValidUser,
      (request, response) => {
        try {
          updateNoteComplete(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });

  expressApp.delete('/api/notes/:id',
      authMiddleware,
      isUserExists,
      isNoteExists,
      isValidUser,
      (request, response) => {
        try {
          deleteNote(request, response);
        } catch (error) {
          response.status(500).json({error: 'Internal server error'});
        }
      });
};
