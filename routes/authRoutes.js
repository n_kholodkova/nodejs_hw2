const {signUp, signIn} = require('./../controllers/auth');

module.exports = (expressApp) => {
  expressApp.post('/api/auth/register', (request, response) => {
    try {
      signUp(request, response);
    } catch (error) {
      response.status(500).json({error: 'Internal server error'});
    }
  });

  expressApp.post('/api/auth/login', (request, response) => {
    try {
      signIn(request, response);
    } catch (error) {
      response.status(500).json({error: 'Internal server error'});
    }
  });
};
