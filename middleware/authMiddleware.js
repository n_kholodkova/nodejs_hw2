const jwt = require('jsonwebtoken');
require('dotenv').config();

const authMiddleware = (async (request, response, next) => {
  let JWTtoken = request.headers['authorization'] ??
    request.headers['Authorization'] ??
    null;
  if (!JWTtoken) {
    return response.status(400).json({
      message: 'No access token.',
    }).end();
  }
  if (JWTtoken.split(' ').length === 2) {
    JWTtoken = JWTtoken.split(' ')[1];
  }
  jwt.verify(
      JWTtoken, process.env.JWT_SECRET, function(error, decoded) {
        if (error) {
          return response
              .status(400)
              .json({message: 'Invalid authentication token.'});
        }
        request.userId = decoded.id;
        next();
      });
});

module.exports.authMiddleware = authMiddleware;
