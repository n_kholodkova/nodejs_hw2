const {UserModel} = require('../models/user');
const {NoteModel} = require('../models/note');

const isUserExists = (async (request, response, next) => {
  const id = request.userId;
  try {
    const user = await UserModel.findOne({_id: id});
    if (!user) {
      return response.status(400).json({message: 'Invalid access token'}).end();
    }
    next();
  } catch (error) {
    return response.status(400).json({message: 'Invalid access token'}).end();
  }
});

const isNoteExists = (async (request, response, next) => {
  const {id} = request.params;
  try {
    const note = await NoteModel.findOne({_id: id});
    if (!note) {
      return response.status(400)
          .json({message: `Note with id ${id} does not exists`}).end();
    }
    next();
  } catch (error) {
    return response.status(400)
        .json({message: `Note with id ${id} does not exists`}).end();
  }
});

const isValidUser = (async (request, response, next) => {
  const {id} = request.params;
  const userId = request.userId;
  try {
    const note = await NoteModel.findOne({_id: id});
    if (note.userId !== userId) {
      return response.status(400)
          .json({message: `Note with id ${id} does not belongs to user`}).end();
    }
    next();
  } catch (error) {
    return response.status(400)
        .json({message: `Note with id ${id} does not exists`}).end();
  }
});

module.exports.isUserExists = isUserExists;
module.exports.isNoteExists = isNoteExists;
module.exports.isValidUser = isValidUser;
